# Test version of scraping Riot LCS match data via the official Riot API
# From basic test cases, this proves inadequte due to the lack of Riot API functionailty

import requests
import json
import sys # Write to file

riot_apiKey = "RGAPI-6ac4c732-0f59-447c-ab5d-f8b0c6d4f0f2"
riot_apiFormat = "?api_key=" + riot_apiKey
riot_regionOCE = "https://oc1.api.riotgames.com"
riot_regionEuro = "https://eue1.api.riotgames.com"

riot_getMatch = "/lol/match/v4/matches/"
riot_tournamentCode = "e1e96873-55a3-4a91-8def-e4fe9d461538"
riot_tournamentData = "/lol/match/v4/matches/by-tournament-code/" + riot_tournamentCode + "/ids"


# Riot get match history format
# https://oc1.api.riotgames.com/lol/match/v4/matches/{matchId}?api_key={riot_api}
# Get a match ID
def getMatchViaID(matchID, key):
  url = riot_regionOCE + riot_getMatch + matchID + key
  response = requests.get(url)
  return response.json()

def main():
  matchId = 1002450118
  matchData = getMatchViaID(str(matchId), riot_apiFormat)
  # print(json.dumps(gameData, indent=4))

  response = requests.get( riot_regionEuro + riot_tournamentData + riot_apiFormat)
  print(response)

  # Write output a file
  matchFile = open("testJson.json", "w")
  matchFile.write(json.dumps(matchData, indent=4))
  matchFile.close()

if __name__ == "__main__":
  main()
