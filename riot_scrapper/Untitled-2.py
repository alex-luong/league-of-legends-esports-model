#%%
import pickle as pickle
import pandas as pd

#%%
pickleFileName = "riot_tourneyDF"

#%%
# Set losing team
df["LosingTeam"] = df.apply(lambda row: row.Team_1 if( row.WinningTeam != row.Team_1) else row.Team_2, axis=1)

#%%
df = pd.read_pickle(pickleFileName)

#%%
# Print DF, showing all rows
pd.option_context("display.max_rows", None)
pd.option_context("display.max_columns", None)

print(df.head(10))

#%% 
# Get occurances of a specific team
teamToFind = "Misfits"
col = "Team_1"
numWins = 0

# Get all rows where teamToFind is found in column col
print(df[df[col].isin([teamToFind])])
tempDF = df[df[col].isin([teamToFind])]

#%%
# Check num wins for my intended team
for winTeamCheck in tempDF.WinningTeam:
  if winTeamCheck == teamToFind:
    numWins = numWins + 1
print(numWins)

#%%
df.to_pickle("pickleFileName")


