"""
  Webscrape the official LOL esports information website for historial match data
"""

from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup

import pickle # Efficient means to store pandas DF
import pandas as pd
import requests
import sys # Write to file
import re # Regex

class Riot_TourneyInfo:
  def __init__(self, site):
    self.site = site
    self.listDictAllTeamData = []

  # Get game match bracket data
  def getBracketData(self, soupObj, currLeague, currTourney):
    teamCounter = 0

    # Get children objs in brackets
    listBracketChildren = list(soupObj.children)

    # Go over the columns of data, 2 sets, 1 set = 2 games
    for outerMostBracket in listBracketChildren:
      # Loop over all these children and for every children
      # Get children in brackets, this would contain 1 or more match data boxes
      listInnerBracketChildren = list(outerMostBracket.children)

      # Go over each inner section, the column splits into two parts
      for teamData in listInnerBracketChildren:
        teamA = ""
        teamAScore = -1
        teamB = ""
        teamBScore = -1

        # Ensure this is a proper match (No Bye)
        checkBye = teamData.find(class_="is-disabled")
        teamData = teamData.find(class_="fixture__top")

        if( checkBye == None):
          # Go over the indivdual teams
          for getTeamDataEle in teamData:

            if( teamCounter%2 == 0):
              # Get their team scores, but for both teams in this, loop again
              # Get Team scores from inner classes
              indivMatchResTeams = getTeamDataEle.find(class_="fixture__team")
              indivMatchResTeams_TeamName = indivMatchResTeams.find(class_="team")
              indivMatchResTeams_TeamName = str(indivMatchResTeams_TeamName["href"])
              indivMatchResTeams_Score = int(indivMatchResTeams.find(class_="score").get_text())

              # Extract only teamname from riot teamname format
              teamA = self.extractTeamName(indivMatchResTeams_TeamName)
              teamAScore = indivMatchResTeams_Score
            else:
              # Get their team scores, but for both teams in this, loop again
              # Get Team scores from inner classes
              indivMatchResTeams = getTeamDataEle.find(class_="fixture__team")
              indivMatchResTeams_TeamName = indivMatchResTeams.find(class_="team")
              indivMatchResTeams_TeamName = str(indivMatchResTeams_TeamName["href"])
              indivMatchResTeams_Score = int(indivMatchResTeams.find(class_="score").get_text())

              # Extract only teamname from riot teamname format
              teamB = self.extractTeamName(indivMatchResTeams_TeamName)
              teamBScore = indivMatchResTeams_Score

            teamCounter = (teamCounter + 1)%2

          # Make dict of lists for match data
          # Format {"League": [LEC, LEC], "Tourney": ["2019 EU Spring", "2018 EU Summer"], 
          # "Team_1": [Misfits, Vitaility], "Score_1" [3, 0]}
          matchDict = { "Team_1": teamA, "Score_1": teamAScore,"Team_2": teamB, "Score_2": teamBScore, "League": currLeague, "Tourney": currTourney}
          self.listDictAllTeamData.append(matchDict)

  # Webscrape all the official LOL esports page seasons and games
  def getAllLeagueTourneyGames(self):
    siteLink = self.site

    # Connect to the site
    driver = webdriver.Firefox()
    # Wait for the page to load first
    driver.implicitly_wait(30)
    driver.get(siteLink)

    # Click League dropdown
    league_dropdown = driver.find_elements_by_class_name("js-league-select")[1]
    league_dropdown.click()

    # Get list of clickable Leagues and it's size
    python_inactiveList = league_dropdown.find_element_by_class_name("selectize-dropdown-content")
    python_ListOpt = python_inactiveList.find_elements_by_class_name("option")
    league_dropdown.click()
    driver.implicitly_wait(1)

    # Iterate over all the leagues and click on them
    # for i in range(0, len(python_ListOpt)):
    for i in range(0, 2):
      league_dropdown = driver.find_elements_by_class_name("js-league-select")[1]
      league_dropdown.click()
      driver.implicitly_wait(1)

      # Get list of clickable Leagues
      python_inactiveList = league_dropdown.find_element_by_class_name("selectize-dropdown-content")
      python_ListOpt = python_inactiveList.find_elements_by_class_name("option")
      python_ListOpt[i].click()  

      # Ensure page is fully loaded before proceeding
      driver.implicitly_wait(3)

      # Click the Tournaments button to open drop down menu
      python_Tourneydropdown = driver.find_elements_by_class_name("js-tournament-select")[1]
      python_Tourneydropdown.click()
      driver.implicitly_wait(1)

      # Get list of tournament options and it's size
      python_inactiveDropDown = python_Tourneydropdown.find_element_by_class_name("selectize-dropdown-content")
      python_listOptions = python_inactiveDropDown.find_elements_by_class_name("option")
      python_Tourneydropdown.click()
      driver.implicitly_wait(2)

      # Iterate over all tournies and click each one, grabbing the match data inside
      for j in range(0, len(python_listOptions)):
        # Click open tournaments tab
        python_Tourneydropdown = driver.find_elements_by_class_name("js-tournament-select")[1]
        python_Tourneydropdown.click()

        # ADD EXPLICIT WAIT HERE
        try:
          WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.CLASS_NAME, "selectize-dropdown-content")))
        except:
          print("No dropdown option avalible")

        # Get list of tournament options and click on the next tourney
        python_inactiveDropDown = python_Tourneydropdown.find_element_by_class_name("selectize-dropdown-content")
        python_listOptions = python_inactiveDropDown.find_elements_by_class_name("option")
        python_listOptions[j].click()
        driver.implicitly_wait(2)

        # Explicitly wait until brackets class has been found
        try:
          WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.CLASS_NAME, "brackets")))
        except:
          print("Can't find the class name")

        # Create BeautifulSoup to parse the html
        soup = BeautifulSoup(driver.page_source, "html.parser")
        
        # Get curr Tourney selected, first item in selectize-dropdown-content is leagues, second is tourneys
        python_tourneyLeagueInfo = soup.find_all(class_="selectize-dropdown-content")
        python_currTourney = python_tourneyLeagueInfo[1].find(class_="selected")
        python_currLeague = python_tourneyLeagueInfo[0].find(class_="selected")

        mainMatchData = soup.find(class_="widget--standings")

        try: 
          mainMatchDataBrackets = mainMatchData.find_all(class_="brackets")

          # When there's actual data present, iterate over brackets and get match data
          if( mainMatchDataBrackets != None):
            # Iterate over brackets present
            for k in range(0, len(mainMatchDataBrackets)):
              matchBracketData = mainMatchDataBrackets[k].find(class_="brackets__column-container")

              try: 
                print("CurrLeague and tourney {} {}".format(python_currLeague.get_text(), python_currTourney.get_text()))

                # Convert bracket data to a dict and append to a list
                self.getBracketData( matchBracketData, python_currLeague.get_text(), python_currTourney.get_text())
              except Exception:
                print("Some exception")
          else:
            print("No brackets, go next tourney")
        except:
          print("Contains no brackets, go next tourney")
        
    # Kill firefox instance
    driver.quit()

  # Extract groupstage lol data from liquidpedia
  # Only to scrape tables
  def extractGroupStageData(self, site):
    response = requests.get(site)

    # Returns a 200 status code
    if response.status_code == requests.codes.ok:
      soup = BeautifulSoup(response.content, "html.parser")
      siteTitle = soup.title.get_text()

      print("siteTitle: {}".format(siteTitle))

      # Extract League and Tourney frm the site title
      leagueTourneyTuple = self.extractLeagueTourney_FrmTitle(siteTitle)
      print("league {}, tourney {}".format(leagueTourneyTuple[0], leagueTourneyTuple[1]))

      # Get the template boxes that hold tables
      templateTableBoxes = soup.find_all(class_="template-box")
      for singleTemplateBox in templateTableBoxes:
        
        # Get each row inside each table "match-row"
        allTableRows = singleTemplateBox.find_all("tr", class_="match-row")
        for singleRow in allTableRows:
          try:
            self.extractTeamDataGroupStages(singleRow, leagueTourneyTuple)
          except:
            print("Error: extracting data from teamRow failed")

    else:
      print("Site not connecting")

  # Get team data from a single row for liquidpedia site
  def extractTeamDataGroupStages(self, htmlTableRows, leagueTourneyTuple):
    counter = 0
    team_1Score = -1
    team_2Score= -1
    team_1Name = ""
    team_2Name = ""

    tableRowChildren = htmlTableRows.find_all("td")
    for indivEle in tableRowChildren:
      # First 2 children are Team_1 name and score, last 2 are Team_2 score and name
      if counter == 0:
        teamNameClass = indivEle.find(class_="team-template-team2-short")
        team_1Name = teamNameClass["data-highlightingclass"]
      # Team_1 Score
      elif counter == 1:
        getScore = list(indivEle.children)
        team_1Score = int(getScore[0])
      # Team_2 Name
      elif counter == 2:
        getScore = list(indivEle.children)
        team_2Score = int(getScore[0])
      # Team_2 Score
      elif counter == 3:
        teamNameClass = indivEle.find(class_="team-template-team-short")
        team_2Name = teamNameClass["data-highlightingclass"]
      else:
        print("else statement")

      counter = counter + 1 
    
    # Ensure empty rows don't get added
    if team_1Score == 0 and team_2Score == 0:
      print("Both scores 0, must be an empty row")
    else:
      # Make dict of lists for match data
      # Format {"League": [LEC, LEC], "Tourney": ["2019 EU Spring", "2018 EU Summer"], 
      # "Team_1": [Misfits, Vitaility], "Score_1" [3, 0]}
      matchDict = { "Team_1": team_1Name, "Score_1": team_1Score,"Team_2": team_2Name, "Score_2": team_2Score, "League": leagueTourneyTuple[0], "Tourney": leagueTourneyTuple[1]}
      self.listDictAllTeamData.append(matchDict)

  # Extracts the Leauge and Tourney from site title, FORMAT: LEAGUE TOURNEY - STAGE - LIQUIDPEDIA
  # Returns a tuple, ("LEAGUE", "TOURNEY")
  def extractLeagueTourney_FrmTitle(self, titleStr):
    league = ""
    tourney = ""
    
    locFirstDash = titleStr.find("-")
    titleStr = titleStr[:(locFirstDash-1)]

    # Find EU LCS, set League to LEC
    if titleStr.find("EU") != -1:
      league = "LEC"

      try:
        titleStr = titleStr.replace("EU LCS", "")
      except:
        print("EU LCS doesn't exist in titleStr")

      # Use Regex to find the year
      theYear = re.search(r"(\d)+", titleStr)
      theYear = theYear.group()

      # Remove the year
      titleStr = titleStr.replace(theYear, "")
      titleStr = titleStr.lstrip()
      titleStr = titleStr.rstrip()

      # Remake the string into a proper tourney str
      tourney = "{} {} {}".format(league, theYear, titleStr)
      return (league, tourney)
    elif titleStr.find("LEC") != -1:
      league = "LEC"

      try:
        titleStr = titleStr.replace("LEC", "")
      except:
        print("EU LEC doesn't exist in titleStr")

      # Use Regex to find the year
      theYear = re.search(r"(\d)+", titleStr)
      theYear = theYear.group()

      # Remove the year
      titleStr = titleStr.replace(theYear, "")
      titleStr = titleStr.lstrip()
      titleStr = titleStr.rstrip()

      # Remake the string into a proper tourney str
      tourney = "{} {} {}".format(league, theYear, titleStr)
      return (league, tourney)
    elif titleStr.find("NA LCS") != -1:
      league = "LCS"

      try:
        titleStr = titleStr.replace("NA LCS", "")
      except:
        print("NA LCS doesn't exist in titleStr")

      # Use Regex to find the year
      theYear = re.search(r"(\d)+", titleStr)
      theYear = theYear.group()

      # Remove the year
      titleStr = titleStr.replace(theYear, "")
      titleStr = titleStr.lstrip()
      titleStr = titleStr.rstrip()

      # Remake the string into a proper tourney str
      tourney = "{} {} {}".format(league, theYear, titleStr)
      return (league, tourney)
    else:
      # Must be NA League (cause I won't bother with the rest for now)
      league = "LCS"
      
      try:
        titleStr = titleStr.replace("LCS", "")
      except:
        print("LCS doesn't exist in titleStr")

      # Use Regex to find the year
      theYear = re.search(r"(\d)+", titleStr)
      theYear = theYear.group()

      # Remove the year
      titleStr = titleStr.replace(theYear, "")
      titleStr = titleStr.lstrip()
      titleStr = titleStr.rstrip()

      # Remake the string into a proper tourney str
      tourney = "{} {} {}".format(league, theYear, titleStr)
      return (league, tourney)

  # Create Pandas DataFrame from list of dictionaries
  def createPandasDF(self):
    # Create PD Dataframe from all scrapped data
    gameDataFrame = pd.DataFrame(self.listDictAllTeamData)

    # Ignore winning team if there's an error
    try: 
      gameDataFrame["WinningTeam"] = gameDataFrame.apply(lambda row: row.Team_1 if(row.Score_1 > row.Score_2) else row.Team_2, axis=1)
    except Exception as e:
      print("WinningTeam pandas DF exception {}".format(e))

    return gameDataFrame

  # Extract team name from format /some/text/TEAMNAME
  def extractTeamName(self, teamString):
    slashPos = teamString.rfind('/', 0, len(teamString))

    teamString = teamString[(slashPos + 1):]
    teamString = teamString.capitalize()

    return str(teamString)

  # Concat exisitng DF together
  def concatDF(self, df):
    oldDF = pd.read_pickle("riot_tourneyDF")
    oldDF = oldDF.append(df, ignore_index=True)

    return oldDF

  # Save Pandas DataFrame as a pickle
  def outputDF(self, df):
    df.to_pickle("riot_tourneyDF")

  def printList(self, listObj):
    for myList in listObj:
      print(myList)