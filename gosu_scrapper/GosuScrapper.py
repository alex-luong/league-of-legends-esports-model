""" Gosu Scrapper uses archive.org (An internet archive site) to get older pages of gosu lol ranking
  It takes the top 100 resutls (all that is archived) and outputs into a pandas DF
"""

from selenium import webdriver
from selenium.webdriver.support.ui import Select
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup

import pickle # Efficient means to store pandas DF
import pandas as pd
import requests
import re # Regex

class GosuScrapper: 
  def __init__(self):
    self.listDict = []  
  
  # Scrap directly from an internet archive link to gosu archive, only works pre 13/02/2018 links
  def directNetArchiveGosuScrapping(self, site):
    driver = webdriver.Firefox()
    # Wait for the page to load first
    driver.implicitly_wait(10)
    driver.get(site)

    # Get 2 pages of teams (100 teams)
    for i in range(0, 2):
      # Get initial page of teams
      soup = BeautifulSoup(driver.page_source, "html.parser")

      # Get container holding the teams
      teamContainer = soup.find(class_="ranking-list")
      self.getTeamsAndElo(teamContainer)

      # Click the next page
      paginationNext = driver.find_element_by_class_name("pagination-next")
      paginationNext.click()

    # Kill firefox instance
    driver.quit()

  # Iterate over the team's children and get the teamName/elo from each of them
  def getTeamsAndElo(self, soupTeamObj):
    for indivTeams in soupTeamObj.find_all("li"):
      eloScore = indivTeams.find(class_="elo")
      eloScore = int(eloScore.get_text())

      # TeamName is contained in the a tag, find it and get_text()
      teamName = indivTeams.find("a")
      teamName = teamName.get_text()
      teamName = teamName.replace("\n", "")

      # Get rank
      findRank = indivTeams.find(class_="ranking")
      findRank = int(findRank.get_text())

      # Remove numbers and leave only the team
      teamName = teamName.replace(str(eloScore), "")
      teamName = teamName.replace(str(findRank), "")
      teamName = teamName.lstrip()
      teamName = teamName.rstrip()

      teamAndScoreDict = {"TeamName": teamName, "EloScore": eloScore}
      self.listDict.append(teamAndScoreDict)

  # Create a Pandas DF with the teamNames and Scores
  def createPandasDF(self):
    df = pd.DataFrame(self.listDict)

    # Sort values by desecnding elo score
    df = df.sort_values(by="EloScore", ascending=False)
    
    return df