""" 
  Get historical LOL odds from oddsportal.com
    Scrape a leagues results and odds based on an inputted link
"""

#%%
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup

import pickle # Efficient means to store pandas DF
import pandas as pd
import time

class HistOdds_Scrapper:
  def __init__(self):
    self.listDict = []
  
  # Scrape a league's match data, including teamNames, scores and the matches odds
  def scrapeSiteData(self, site, league):

    # Set up selenium driver and connect to a site
    driver = webdriver.Firefox()
    driver.implicitly_wait(30)
    driver.get(site)

    # Get the years class to start navigating
    getYears = driver.find_elements_by_class_name("main-filter")
    getYears = getYears[1]
    yearClickable = getYears.find_elements_by_tag_name("a")

    # Iterate over all the years found
    for i in range(0, len(yearClickable)):
      # Get the years class to start navigating, must get links again to prevent stale links
      getYears = driver.find_elements_by_class_name("main-filter")
      getYears = getYears[1]
      yearClickable = getYears.find_elements_by_tag_name("a")
      
      # Click the curr year
      yearClickable[i].click()
      
      # Explicitly wait until the table containing match data is loaded
      try:
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, "table-main")))
      except:
        print("Can't find the table-main class")

      soup = BeautifulSoup(driver.page_source, "html.parser")

      # Check for additional pages and get how many pages
      if soup.find(id="pagination") != None:
        pagesList = soup.find(id="pagination")
        pagesList = pagesList.find_all("a")

        # Get last ele, contains the last page number
        lastPageNum = pagesList[len(pagesList)-1]
        lastPageNum = int(lastPageNum["x-page"])

        # Iterate over all the pages of results
        for j in range(0, (lastPageNum)-1):
          
          # Get the table results
          theTable = soup.find(class_="table-main")
          self.scrapeTableData(theTable, league)

          # Click on the next page arrow
          pages = driver.find_element_by_id("pagination")
          pagesClickable = pages.find_elements_by_tag_name("a")
          pagesClickable[len(pagesClickable)-2].click()

          # Explicitly wait until the page is loaded again
          try:
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "pagination")))
          except:
            print("Can't find the table-main class")

          time.sleep(3)

      else:
        # Only a single page of results 
        theTable = soup.find(class_="table-main")
        self.scrapeTableData(theTable, league)

    driver.quit()

  # Scrapes the teamnames, scores and odds from the table  
  def scrapeTableData(self, table, league):
    tourney = ""

    # Only gets the season title and match results
    wantedTableRes = table.find_all("tr", class_=["nob-border" ,"deactivate"])

    # Iterate over every row
    for rowRes in wantedTableRes:
      # Check if currEle is the season row
      if "nob-border" in rowRes["class"]:
        rowRes = rowRes.find(class_="first2")
        rowRes = rowRes.get_text()

        # Extract the season only
        season = self.extractSeasonOnly(rowRes)
      else:
        # This must be the row with match results, extract match results
        matchResDict = self.extractTeamData(rowRes)

      # Add each match row into a list of dicts
      # Concatnates 2 dictionaries
      matchResDict = matchResDict.update(season)
      self.listDict.append(matchResDict)
      

  # Date Season format DATE - SEASON
  # Remove everything before the -
  def extractSeasonOnly(self, date_season):
    locDash = date_season.find("-")

    # Ensure the dash was found
    if locDash != -1:
      date_season = date_season[locDash+1:]
      date_season = date_season.lstrip()
      date_season = date_season.rstrip()

    return {"Tourney": date_season}

  # For a singlar match, extract the team1 and team2 names, scores and odds
  def extractTeamData(self, teamData):
    # Get teamNames and scores, both in a single row
    teamNames = teamData.find(class_="table-participant").get_text()
    teamScores = teamData.find(class_="table-score").get_text()

    # Get teamOdds, first value is team1, second is team2
    teamOdds = teamData.find_all(class_="odds-nowrp")
    team1Odds = float(teamOdds[0].get_text())
    team2Odds = float(teamOdds[1].get_text())

    # Extract team1 and team2 names
    locDash = teamNames.find("-")
    team1Name = teamNames[:locDash-1]
    team2Name = teamNames[locDash+1:]

    # Remove any trailing or leading whitespace
    team1Name = team1Name.lstrip()
    team1Name = team1Name.rstrip()
    team2Name = team2Name.lstrip()
    team2Name = team2Name.rstrip()

    # Extract Team1 and team2 scores, FORMAT NUM:NUM
    locColon = teamScores.find(":")
    team1Score = int(teamScores[:locColon-1])
    team2score = int(teamScores[locColon+1:])

    return {"Team_1": team1Name, "Score_1": team1Score, "Team_1_Odds": team1Odds ,"Team_2": team2Name, "Score_2": team2score, "Team_2_Odds": team2Odds}

      





