# Scrape riot's lolesports site https://eu.lolesports.com/en/standings# and get the match link
# from lec tournaments, use these match links to get a json file of the game. 
# Use this to build who vsed who in what tournament and the results
#
# Compare results with Gosu's rankings and Betting odds and begin discovering trends in the data

from riot_scrapper.Riot_TourneyInfo import Riot_TourneyInfo
from gosu_scrapper.GosuScrapper import GosuScrapper
from hist_odds.HistOdds_Scrapper import HistOdds_Scrapper

import pandas as pd

def main():
  lolesportSite = "https://eu.lolesports.com/en/standings"
  gosuSite = "https://web.archive.org/web/20180118112423/https://www.gosugamers.net/lol/rankings"
  histOddsSite = "https://www.oddsportal.com/esports/world/league-of-legends-championship-series/results/"

  lolGroupSitesList = [
    "https://liquipedia.net/leagueoflegends/LEC/2019/Spring/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/Europe/2018/Summer/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/Europe/2018/Spring/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/Europe/2017/Summer/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/Europe/2017/Spring/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/Europe/2016/Summer/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/Europe/2016/Spring/Group_Stage"
    "https://liquipedia.net/leagueoflegends/LCS/2019/Spring/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/North_America/2018/Summer/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/North_America/2018/Spring/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/North_America/2017/Summer/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/North_America/2017/Spring/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/North_America/2016/Summer/Group_Stage",
    "https://liquipedia.net/leagueoflegends/LCS/North_America/2016/Spring/Group_Stage"
  ]

  # Initalise classes
  riotTourneyObj = Riot_TourneyInfo(lolesportSite)
  gosuObj = GosuScrapper()
  histOddsObj = HistOdds_Scrapper()
  
  # Connect to HTML and get beautiful soup obj to extract info
  riotTourneySoup = riotTourneyObj.getAllLeagueTourneyGames()
  df = riotTourneyObj.createPandasDF()
  df = riotTourneyObj.concatDF(df)
  riotTourneyObj.outputDF(df)

  # Scrape groupstages from liquidpedia
  for link in lolGroupSitesList:
    print(link)
    riotTourneyObj.extractGroupStageData(link)

  df = riotTourneyObj.createPandasDF()

  # # Concatinate pandasDF with old DF and output as riot_tourneyDF
  df = riotTourneyObj.concatDF(df)

  riotTourneyObj.outputDF(df)

  # Gosu Scrapping
  gosuObj.directNetArchiveGosuScrapping(gosuSite)
  df = gosuObj.createPandasDF()
  df.to_pickle("gosu_data")

  # Historical odds scrapping
  histOddsObj.scrapeSiteData(histOddsSite, "LCS")


if __name__ == "__main__":
  main()